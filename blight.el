;;; blight.el --- Control system backlight       -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Ian Eure

;; Author: Ian Eure <ian@retrospec.tv>
;; Keywords: hardware
;; Version: 1.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Blight is an Emacs package to control display brightness.

;;; Code:

(require 'cl-generic)
(require 'subr-x)
(require 'eieio)

;;;###autoload
(defclass blight ()
  ((min :type integer :initarg :min :initform 1
        :documentation "Minimum brightness Blight will set.  If changed to 0, the backlight can turn completely off.")
   (current :type number
            :documentation "Current brightness, in percent from 0-100."
            :reader blight-get
            :writer blight-set)
   (last-changed :type float
                 :documentation "The time of the last change to this backlight."))

  :documentation "Base class representing a backlight."
  :abstract t)

(cl-defmethod initialize-instance :after ((this blight) &rest _)
  (setf (slot-value this 'last-changed) (float-time)))

(cl-defmethod blight-get ((this blight) &optional force)
  "Get the brightness of backlight THIS.

When FORCE is non-NIL, read the value from hardware.  Otherwise,
return the currently cached value."
  (with-slots (current) this
    current))

(cl-defmethod blight-set ((this blight) percent)
  "Set backlight THIS to PERCENT brightness."
  (cl-assert (<= 0 percent 100) t "Can only set brightness between 0%-100%")
  (with-slots (current last-changed) this
    (setf last-changed (float-time)
          current percent)))

(cl-defmethod blight-step ((this blight) &optional (amount 10))
  "Return a command to adjust backlight THIS by AMOUNT percent."
  (lambda ()
    (interactive)
    (with-slots (current min) this
      (let ((qoff (% current (abs amount))))
        (blight-set this
                    (thread-first (+ amount (- current qoff))
                      (min 100)
                      (max min)))))
    (when (and (boundp 'lemon-mode) lemon-mode)
      (lemon-display))))

(provide 'blight)
;;; blight.el ends here
