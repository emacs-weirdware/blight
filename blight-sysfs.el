;;; blight-sysfs.el --- Backlight control for SysFS  -*- lexical-binding: t; -*-

;; Copyright (C) 2021  Ian Eure

;; Author: Ian Eure <ian@retrospec.tv>
;; Keywords: hardware

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'cl-generic)
(require 'eieio)
(require 'blight)

(defconst blight-sysfs--base "/sys/class/backlight"
  "SysFS base directory for backlights.")

(defun blight-sysfs--backlights ()
  "Return a list of all available backlights."
  (directory-files "/sys/class/backlight" t "^[^\.]"))

;;;###autoload
(defclass blight-sysfs (blight)
  ((backlight :type string :initarg :backlight
         :initform (car (blight-sysfs--backlights))
         :documentation "SysFS directory for this backlight.")
   (step :type number
         :documentation "Step value equivalent to a 1% change."))
  :documentation "Class representing a backlight controlled with Linux SysFS.")

(cl-defgeneric blight-sysfs--path ((this blight-sysfs) attr)
  "Return the absolute path to ATTR for backlight THIS."
  (with-slots (backlight) this
    (format "%s/%s" backlight attr)))

(defun blight-sysfs--read (file)
  "Read the contents of FILE, returning a number."
  (with-temp-buffer
    (insert-file-contents file)
    (string-to-number (buffer-substring (point-min) (point-max)))))

(cl-defmethod initialize-instance :after ((this blight-sysfs) &rest _)
  (with-slots (step) this
    (setf step (/ (blight-sysfs--read (blight-sysfs--path this "max_brightness")) 100.0)))
  (blight-get this))

(cl-defmethod blight-get ((this blight-sysfs) &optional force)
  "Get the brightness of backlight THIS.

When FORCE is non-NIL, read the value from SysFS.  Otherwise, return
the last cached value."
  (with-slots (current step) this
    (if (or force (not (slot-boundp this 'current)))
        (setf current (round (/ (blight-sysfs--read (blight-sysfs--path this "brightness")) step)))
      current)))

(cl-defmethod blight-set :after ((this blight-sysfs) percent)
  "Set backlight THIS to PERCENT brightness."
  (with-slots (step) this
    (with-temp-buffer
      (insert (format "%s" (round (* step percent))))
      (write-region nil nil (blight-sysfs--path this "brightness") nil 'silent))))

(provide 'blight-sysfs)
;;; blight-sysfs.el ends here
