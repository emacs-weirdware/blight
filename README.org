* Blight
  :PROPERTIES:
  :ID:       1c512579-7c8d-4464-8e47-fc87f6ba10e8
  :END:

  Blight is an Emacs package to control display brightness.
  Significant frustration with [[https://github.com/mschuldt/backlight.el][=backlight.el=]] drove me to write my
  own.


** Features
   :PROPERTIES:
   :ID:       2a918a6f-c833-451b-b174-09cbb6533a1b
   :END:

   - Object-oriented code using EIEIO.
   - Has a base class implementing a reasonable API which focuses on
     the extremely basic "set the back light to this percentage"
     functionality instead of advanced tightly-coupled frippery.
   - Includes a concrete implementation that uses SysFS to control
     brightness.
   - Other systems (D-Bus, macOS, Windows, =xbacklight=, XELB using
     XRandR) are easily supportable, giving the same experience across
     environments.
   - 500% snappier name than any other backlight tool for Emacs.


** Installation
   :PROPERTIES:
   :ID:       9a1afe44-a775-4a90-b0c9-9ef3da7bb11b
   :END:

   Use [straight.el](https://github.com/radian-software/straight.el).

   You’ll want to set up the
   [straight-weirdware](https://codeberg.org/emacs-weirdware/straight-weirdware)
   recipe repository first.  Then, add this to your Emacs configuration:

   #+BEGIN_SRC emacs-lisp
	 (use-package blight
	   :demand t

	   :config
	   (setq blight-global (blight-sysfs))
	   (exwm-input-set-key (kbd "<XF86MonBrightnessUp>") (blight-step blight-global 5))
	   (exwm-input-set-key (kbd "<XF86MonBrightnessDown>") (blight-step blight-global -5)))
   #+END_SRC


*** udev rules
	:PROPERTIES:
	:ID:       06b1bf42-ad0b-47af-bc25-9389fc6508de
	:END:

	For brightness to work, your user account needs write access to
	the files in sysfs which control the display brightness.  The
	[bundled udev rules](20-backlight.rules) will grant write access
	to the =video= group -- install those rules, and make sure your
	user is in that group, and you’ll be set.

** API
   :PROPERTIES:
   :ID:       c0adfa8a-47ef-4b53-a337-56ede19ea813
   :END:

*** Create a SysFS instance
    :PROPERTIES:
    :ID:       7e923fb6-0dd6-4402-bd8e-ac0a6761b37a
    :END:

    Each Blight instance controls one backlight.  By default, it picks
    the first entry from =/sys/class/backlight=, which is the only
    entry on most systems.

    #+BEGIN_SRC emacs-lisp :exports code
      (setq my/blight (blight-sysfs))
    #+END_SRC

    #+RESULTS:
    : #s(#s(eieio--class blight-sysfs "Class representing a backlight controlled with Linux SysFS." (#s(eieio--class blight "Base class representing a backlight." nil [#s(cl-slot-descriptor min 1 integer ((:documentation . "Minimum brightness Blight will set.  If changed to 0, the backlight can turn completely off.") (:group quote blight))) #s(cl-slot-descriptor current unbound number ((:documentation . "Current brightness, in percent from 0-100."))) #s(cl-slot-descriptor last-changed unbound list ((:documentation . "The time of the last change to this backlight.")))] #s(hash-table size 65 test eq rehash-size 1.5 rehash-threshold 0.8125 data (min 0 current 1 last-changed 2)) (blight-sysfs) ((:min . min)) [] [] #s(#3 1 unbound unbound) (:custom-groups (quote blight) :documentation "Base class representing a backlight." :abstract t))) [#s(cl-slot-descriptor min 1 integer ((:documentation . "Minimum brightness Blight will set.  If changed to 0, the backlight can turn completely off.") (:group quote blight))) #s(cl-slot-descriptor current unbound number ((:documentation . "Current brightness, in percent from 0-100."))) #s(cl-slot-descriptor last-changed unbound list ((:documentation . "The time of the last change to this backlight."))) #s(cl-slot-descriptor base (car (blight-sysfs--backlights)) string ((:documentation . "Base SysFS directory for this backlight.") (:group quote blight))) #s(cl-slot-descriptor step unbound number ((:documentation . "Step value equivalent to a 1% change.")))] #s(hash-table size 65 test eq rehash-size 1.5 rehash-threshold 0.8125 data (min 0 current 1 last-changed 2 base 3 step 4)) nil ((:min . min)) [] [] #s(#1 1 unbound unbound "/sys/class/backlight/intel_backlight" unbound) (:custom-groups (quote blight) :documentation "Class representing a backlight controlled with Linux SysFS.")) 1 30 unbound "/sys/class/backlight/intel_backlight" 68.18)

    You can specify a different SysFS backlight with:

    #+BEGIN_SRC emacs-lisp
      (setq my/blight (blight-sysfs :backlight "/sys/class/backlight/intel_backlight"))
    #+END_SRC


*** Get brightness
    :PROPERTIES:
    :ID:       553d4405-34b1-4df7-9474-40416ccd610b
    :END:

    #+BEGIN_SRC emacs-lisp :exports both
      (blight-get my/blight)
    #+END_SRC

    #+RESULTS:
    : 30


*** Set brightness
    :PROPERTIES:
    :ID:       c59fbcea-3896-4816-b0c2-002ae2f90f4b
    :END:

    The argument is the brightness percentage to set, from 0-100.

    #+BEGIN_SRC emacs-lisp :exports both
      (blight-set my/blight 50)
    #+END_SRC

    #+RESULTS:
    : 50


*** Step brightness
    :PROPERTIES:
    :ID:       5910ab15-edf9-4df7-80d1-077a91ba1732
    :END:

    The =blight-step= function returns a command that changes the
    brightness by the specified amount.

    #+BEGIN_SRC emacs-lisp :exports code
      (blight-step my/blight 10)              ; Increase by 10%
      (blight-step my/blight -10)             ; Decrease by 10%
    #+END_SRC

    By default, Blight won’t let you step the brightness to zero,
    since this can be surprising (you can still set it to 0 with
    =blight-set=).  The lowest brightness you can set may be specified
    with the =:min= argument to the Blight constructor:

    #+BEGIN_SRC emacs-lisp
      ;; Allow stepping the backlight completely off.
      (blight-sysfs :min 0)

      ;; Never go below 10% brightness.
      (blight-sysfs :min 10)
    #+END_SRC

** EXWM Configuration
   :PROPERTIES:
   :ID:       5a6c86a7-d6e3-46eb-8d0f-9fd64dcdbb88
   :END:

   #+BEGIN_SRC emacs-lisp :eval none :exports code
     (use-package blight
       :defer t
       :after exwm
       :straight (blight :repo "ssh://git@gitlab.com/ieure/blight.git")
       :init
       (setq my/blight (blight-sysfs))
       (exwm-input-set-key (kbd "<XF86MonBrightnessUp>") (blight-step my/blight 10))
       (exwm-input-set-key (kbd "<XF86MonBrightnessDown>") (blight-step my/blight -10)))
   #+END_SRC
